{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit pkCheck;

{$warn 5023 off : no warning about unused units}
interface

uses
  untsalescheck, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('untsalescheck', @untsalescheck.Register);
end;

initialization
  RegisterPackage('pkCheck', @Register);
end.
